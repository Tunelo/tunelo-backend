//Load configs
var defaults = require("./config/defaults.json")
//Load local modules
//Load third party modules
var chalk = require("chalk")
//Define modules
function log(type, message) {
    var colour;
    switch(type) {
        case "state":
            colour = "bgBlue"
            break;
        case "warning": 
            colour = "bgYellow"
            break;
        case "error":
            colour = "bgRed"
            break;
        case "ok":
            colour = "bgGreen"
            break;
        case "highlight": 
            colour = "bgMagenta"
            break;
        default:
            colour = "white"
            break;
    }
    console.log(chalk.grey("[" + defaults.defaults.PRODUCT_NAME.toUpperCase() + "_" + defaults.versions[defaults.env] + "] ") + chalk[colour]("[" + type.toUpperCase() + "]") + " " + message)
}

log("state", "[START] Gooooob morning! Starting " + defaults.defaults.PRODUCT_NAME)